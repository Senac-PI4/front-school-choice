export class Usuario{
    email: String;
	senha: String;
	tipoUsuario: Number;
	nome: String;
	sobrenome: String;
	cpf: String;
	genero: String;
	telefone: String;
	dataNascimento: Date;
	estadocivil: String;
	cep: String;
	logradouro: String;
	numero: Number;
	complemento: String;
}
