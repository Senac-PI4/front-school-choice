import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../model/usuario';
import { HttpClient } from '@Angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  listarUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>('http://localhost:8080/school-choice-app/servicos/usuario');
  }

  addUsuario(usuario: Usuario): Observable<any> {
    return this.http.post('http://localhost:8080/school-choice-app/servicos/usuario', usuario);
  }

  alterarUsuario(usuario: Usuario): Observable<any> {
    return this.http.put('http://localhost:8080/school-choice-app/servicos/usuario', usuario);
  }

  excluirUsuario(id: number): Observable<any> {
    return this.http.delete('http://localhost:8080/school-choice-app/servicos/usuario/' + id);
  }

}
