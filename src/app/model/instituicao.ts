export class Instituicao{
    instituicao_id: Number;
	nomeInstituicao: String;
	cnpj: String;
	telefone: String;
	email: String;
	logradouro: String;
	numero: Number;
	complemento: String;
	notaMec: Number;
	dataAgendamento: Date;
	horaAgendamento: String;
}